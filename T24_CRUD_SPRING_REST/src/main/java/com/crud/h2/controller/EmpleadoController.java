package com.crud.h2.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.h2.dto.Empleado;
import com.crud.h2.service.EmpleadoServiceImpl;

@RestController
@RequestMapping("/api")
public class EmpleadoController {

	@Autowired
	EmpleadoServiceImpl empleadoServiceImpl;
	
	@GetMapping("/empleados")
	public List<Empleado> listarEmpleado(){
		return empleadoServiceImpl.listarEmpleados();
	}
	
	@PostMapping("/empleados")
	public Empleado guardarEmpleado(@RequestBody Empleado empleado) {
		empleado.setSalario(salarioEmpleado(empleado.getNombreTrabajo()));
		return empleadoServiceImpl.guardarCliente(empleado);
	}
	
	@GetMapping("/empleados/{id}")
	public Empleado empleadoPorId(@PathVariable(name="id") Long id) {
		Empleado empXid = new Empleado();
		empXid = empleadoServiceImpl.empleadoPorID(id);
		return empXid;		
	}
	
	@PutMapping("/empleados/{id}")
	public Empleado updateEmpleado(@PathVariable(name="id") Long id, @RequestBody Empleado empleado) {
		Empleado empleado_selecionado = new Empleado();
		Empleado empleado_actualizado = new Empleado();
		
		
		empleado_selecionado = empleadoServiceImpl.empleadoPorID(id);
		
		empleado_selecionado.setApellido(empleado.getApellido());
		empleado_selecionado.setNombre(empleado.getNombre());
		empleado_selecionado.setNombreTrabajo(empleado.getNombreTrabajo());
		
		empleado_selecionado.setSalario(salarioEmpleado(empleado.getNombreTrabajo()));
		
		empleado_actualizado = empleadoServiceImpl.updateEmpleado(empleado_selecionado);
		
		return empleado_actualizado;
	}
	
	@DeleteMapping("/empleados/{id}")
	public void eliminarEmpleado(@PathVariable(name="id")Long id) {
		empleadoServiceImpl.eliminarEmpleado(id);
	}
	
	//Devolvemos un string con el salario del empleado a partir del nombre del empleo
	public String salarioEmpleado(String empleo) {
		String salarioEmpleo= null;
		
		switch (empleo) {
			case "informatica":
				salarioEmpleo = "1000";
			break;
			case "administrativo":
				salarioEmpleo = "2200";
			break;
			case "medico":
				salarioEmpleo = "2600";
			break;
		}
		return salarioEmpleo;
	}
}
