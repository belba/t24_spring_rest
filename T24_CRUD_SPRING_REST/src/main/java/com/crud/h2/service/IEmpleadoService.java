package com.crud.h2.service;

import java.util.List;

import com.crud.h2.dto.Empleado;

public interface IEmpleadoService {
	
	//Metodo del CRUD
	public List<Empleado> listarEmpleados();
	
	public Empleado guardarCliente(Empleado empleado);
	
	public Empleado empleadoPorID(Long id);
	
	public Empleado updateEmpleado(Empleado empleado);
	
	public void eliminarEmpleado(Long id);
	
}
