package com.crud.h2.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.h2.dao.IEmpleadoDAO;
import com.crud.h2.dto.Empleado;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService{

	@Autowired
	IEmpleadoDAO iEmpleadoDao;
	
	@Override
	public List<Empleado> listarEmpleados() {
		// TODO Auto-generated method stub
		return iEmpleadoDao.findAll();
	}

	@Override
	public Empleado guardarCliente(Empleado empleado) {
		// TODO Auto-generated method stub
		return iEmpleadoDao.save(empleado);
	}

	@Override
	public Empleado empleadoPorID(Long id) {
		// TODO Auto-generated method stub
		return iEmpleadoDao.findById(id).get();
	}

	@Override
	public Empleado updateEmpleado(Empleado empleado) {
		// TODO Auto-generated method stub
		return iEmpleadoDao.save(empleado);
	}

	@Override
	public void eliminarEmpleado(Long id) {
		// TODO Auto-generated method stub
		iEmpleadoDao.deleteById(id);	
	}
}
